package main;

import java.sql.SQLException;

import controller.Plattform;
import db.Datenbank;
import view.FinanzSoftware;

public class Main {
	/**
	 * Datenbank-Logindaten werden �ber die Argumente �bergeben.
	 * @param args Datenbankname Username Passwort(optional)
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		if(args.length < 2) {
			System.err.println("Ung�ltige Eingabe");
			return;
		}
		String databaseName = args[0];
		String user = args[1];
		String password = args.length == 3 ? args[2] : "";
		Datenbank db = new Datenbank(databaseName, user, password);
		db.createDB();
		db.fillDBWithTestData();
		FinanzSoftware frame = new FinanzSoftware();
		Plattform plattform = new Plattform(frame, db);
		plattform.einloggen("max@muster.com", "pass123");
		frame.start(plattform);
	}
}
