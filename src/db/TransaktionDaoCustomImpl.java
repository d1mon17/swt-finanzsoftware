package db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import model.Ausgabe;
import model.Bankkonto;
import model.Einnahme;
import model.Transaktion;

/**
 * Implementation vom TransaktionDAO.
 */
public class TransaktionDaoCustomImpl implements TransaktionDao {
	private EinnahmeDao einnahmeDao;
	private AusgabeDao ausgabeDao;
	
	/**
	 * Konstruktor der Klasse TransaktionDaoCustomImpl.
	 * @param einnahmeDao	DAO der Fachklasse Einnahme (Pflicht).
	 * @param ausgabeDao	DAO der Fachklasse Ausgabe (Pflicht).
	 * @throws SQLException
	 */
	public TransaktionDaoCustomImpl(EinnahmeDao einnahmeDao, AusgabeDao ausgabeDao) throws SQLException {
		this.einnahmeDao = einnahmeDao;
		this.ausgabeDao = ausgabeDao;
	}

	@Override
	public List<Transaktion> getAllTransaktionenByKonto(Bankkonto bankkonto) throws SQLException {
		List<Transaktion> transaktionen = new ArrayList<>(einnahmeDao.queryForEq("bankkonto_id", bankkonto.getId()));
		transaktionen.addAll(ausgabeDao.queryForEq("bankkonto_id", bankkonto.getId()));
		bankkonto.setTransaktionen(transaktionen); // alle Transaktionen aus der DB werden dem Bankkonto übergeben.
		return transaktionen;
	}

	@Override
	public void createTransaktion(Transaktion transaktion) throws SQLException {
		if(transaktion instanceof Einnahme) {
			einnahmeDao.create((Einnahme)transaktion);
		}else {
			ausgabeDao.create((Ausgabe)transaktion);
		}
		transaktion.getBankkonto().getTransaktionen().add(transaktion); // neu Transaktion wird dem Bankkonto hinzugefügt.
	}

	@Override
	public void createTransaktionen(Collection<Transaktion> transaktionen) throws SQLException {
		for(Transaktion transaktion : transaktionen) {
			createTransaktion(transaktion);
		}
	}
	
	@Override
	public void removeTransaktion(Transaktion transaktion) throws SQLException {
		transaktion.getBankkonto().getTransaktionen().remove(transaktion);
		if(transaktion instanceof Einnahme) {
			einnahmeDao.delete((Einnahme)transaktion);
		}else {
			ausgabeDao.delete((Ausgabe)transaktion);
		}
	}
	
}
