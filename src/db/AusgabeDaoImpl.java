package db;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import model.Ausgabe;

/**
 * Implementation vom AusgabeDAO.
 */
public class AusgabeDaoImpl extends BaseDaoImpl<Ausgabe, Long> implements AusgabeDao {
	
	/**
	 * Konstruktor der Klasse AusgabeDaoImpl.
	 * @param connectionSource Datenbankverbindung (Pflicht).
	 * @throws SQLException
	 */
	public AusgabeDaoImpl(ConnectionSource connectionSource) throws SQLException {
		super(connectionSource, Ausgabe.class);
	}
}