package db;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import model.Einnahme;

/**
 * Implementation vom EinnahmeDAO.
 */
public class EinnahmeDaoImpl extends BaseDaoImpl<Einnahme, Long> implements EinnahmeDao {
	/**
	 * Konstruktor der Klasse AusgabeDaoImpl.
	 * @param connectionSource Datenbankverbindung (Pflicht).
	 * @throws SQLException
	 */
	public EinnahmeDaoImpl(ConnectionSource connectionSource) throws SQLException {
		super(connectionSource, Einnahme.class);
	}
}
