package db;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import model.Benutzer;
import model.Kategorie;

/**
 * Implementation vom KategorieDAO.
 */
public class KategorieDaoImpl extends BaseDaoImpl<Kategorie, Long> implements KategorieDao {
	/**
	 * Konstruktor der Klasse KategorieDaoImpl.
	 * @param connectionSource Datenbankverbindung (Pflicht).
	 * @throws SQLException
	 */
	public KategorieDaoImpl(ConnectionSource connectionSource) throws SQLException {
		super(connectionSource, Kategorie.class);
	}

	
	@Override
	public List<Kategorie> getAllKategorienByBenutzer(Benutzer benutzer) throws SQLException {
		return queryForEq("benutzer_id", benutzer.getId());
	}
}
