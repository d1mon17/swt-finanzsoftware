package db;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import model.Ausgabe;
import model.Bankkonto;
import model.Benutzer;
import model.Einnahme;
import model.Kategorie;
import model.Transaktion;
import model.builders.BankkontoBuilder;
import model.builders.BenutzerBuilder;

/**
 * Die Klasse Datenbank b�ndelt alle wichtigen Informationen der Datenbank.
 */
public class Datenbank {
	// Datebank Logindaten
	private final String datenbankName;
	private final String user;
	private final String password;
	// Datenbankverbindung
	private ConnectionSource connectionSource;
	// DAO-Klassen
	private BenutzerDao benutzerDao;
	private BankkontoDao bankkontoDao;
	private KategorieDao kategorieDao;
	private EinnahmeDao einnahmeDao;
	private AusgabeDao ausgabeDao;
	private TransaktionDao transaktionDao;
	
	/**
	 * Konstruktor der Klasse Datenbank.<br>
	 * Um Datenbank zu erzeugen benutze createDB().
	 * @param datenbankName Name der Datenbank, die lokal bereits angelegt wurde(Pflicht).
	 * @param user Username, um die Verbindung zur Datenbank aufbauen zuk�nnen(Pflicht).
	 * @param password Passwort, um die Verbindung zur Datenbank aufbauen zuk�nnen(optional).
	 */
	public Datenbank(String datenbankName, String user, String password) {
		this.datenbankName = datenbankName;
		this.user = user;
		this.password = password;
	}
	
	/**
	 * Erzeugt die Datenbank.
	 * @throws SQLException
	 */
	public void createDB() throws SQLException{
		// Baue die Verbindung zur Datenbank auf
		String s = String.format("jdbc:mariadb://localhost/%1$s?user=%2$s&password=%3$s", datenbankName, user, password);
		connectionSource = new JdbcConnectionSource(s);
		// Lege DAO-Klassen an.
		benutzerDao = new BenutzerDaoImpl(connectionSource);
		bankkontoDao = new BankkontoDaoImpl(connectionSource);
		kategorieDao = new KategorieDaoImpl(connectionSource);
		einnahmeDao = new EinnahmeDaoImpl(connectionSource);
		ausgabeDao = new AusgabeDaoImpl(connectionSource);
		transaktionDao = new TransaktionDaoCustomImpl(einnahmeDao, ausgabeDao);
				
				
		// Erzeuge Tabellen in DB falls nicht vorhanden.
		TableUtils.createTableIfNotExists(connectionSource, Benutzer.class);
		TableUtils.createTableIfNotExists(connectionSource, Bankkonto.class);
		TableUtils.createTableIfNotExists(connectionSource, Kategorie.class);
		TableUtils.createTableIfNotExists(connectionSource, Einnahme.class);
		TableUtils.createTableIfNotExists(connectionSource, Ausgabe.class);
	}
	
	/**
	 * F�llt die Datenbank mit Testdaten.<br>
	 * Soll nach createDB() ausgef�hrt werden.
	 * @throws SQLException
	 */
	public void fillDBWithTestData() throws SQLException{
		// Leere alle Tabellen.
		TableUtils.clearTable(connectionSource, Benutzer.class);
		TableUtils.clearTable(connectionSource, Bankkonto.class);
		TableUtils.clearTable(connectionSource, Kategorie.class);
		TableUtils.clearTable(connectionSource, Einnahme.class);
		TableUtils.clearTable(connectionSource, Ausgabe.class);
		
		// F�lle DB mit Testdaten.
				//Benutzer benutzer = new Benutzer("max@mail.de", "123", "Musterman", "Max", "1.1.1990", "Beispielstrasse", "74a", 22222, "Luebeck", "Deutschland");
				Benutzer benutzer = new BenutzerBuilder("max@muster.com", "pass123")
						.gebDatum("1.1.1994")
						.vorname("Max")
						.nachname("Mustermann")
						.land("DE")
						.strasse("Mustermann Stra�e")
						.hausNr("74a")
						.plz(66666)
						.ort("Luebeck")
						.build();
						
				benutzerDao.create(benutzer);
				List<Kategorie> kategoerien = Arrays.asList(
						new Kategorie(benutzer, "Gehalt"),
						new Kategorie(benutzer, "Zinsen"),
						new Kategorie(benutzer, "Miete"),
						new Kategorie(benutzer, "Spesen"),
						new Kategorie(benutzer, "Sondereinnahme"),
						new Kategorie(benutzer, "Ausgleich"),
						new Kategorie(benutzer, "Strom"),
						new Kategorie(benutzer, "Verpflegung"),
						new Kategorie(benutzer, "Werkstatt"),
						new Kategorie(benutzer, "Tanken"),
						new Kategorie(benutzer, "Versicherung"),
						new Kategorie(benutzer, "Kredit"),
						new Kategorie(benutzer, "Unterhalt"),
						new Kategorie(benutzer, "Restaurant"),
						new Kategorie(benutzer, "Freizeit"));
				kategorieDao.create(kategoerien);
				//Bankkonto konto = new Bankkonto(benutzer, "Konto1", 2000.0, "123412341234", "9876543", "BeispielBank");
				Bankkonto konto = new BankkontoBuilder(benutzer, "Hauptkonto", 2000.0)
						.bankname("BeispielBank")
						.bic("123456")
						.iban("12345678912345")
						.build();
				bankkontoDao.create(konto);
				List<Transaktion> transaktionen = Arrays.asList(
						new Einnahme(kategoerien.get(0), konto, 1500.0, "�", null, null, "20.2.2018", 0, null, "Arbeit"),
						new Einnahme(kategoerien.get(0), konto, 2000.0, "�", null, null, "10.3.2018", 0, null, "Arbeit"),
						new Ausgabe(kategoerien.get(3), konto, 150.0, "�", "Fahrtkosten", null, "18.3.2018", 0, null, "Deutsche Bahn"));
				transaktionDao.createTransaktionen(transaktionen);
	}
	/**
	 * Schlie�t die Datenbankverbindung.
	 * @throws IOException
	 */
	public void closeDBConnection() throws IOException{
		connectionSource.close();
	}
	
	public BenutzerDao getBenutzerDao() {
		return benutzerDao;
	}

	public BankkontoDao getBankkontoDao() {
		return bankkontoDao;
	}

	public KategorieDao getKategorieDao() {
		return kategorieDao;
	}

	public TransaktionDao getTransaktionDao() {
		return transaktionDao;
	}

	
}
