package db;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.Dao;

import model.Benutzer;
import model.Kategorie;

/**
 * DAO der Fachklasse Einnahme.
 */
public interface KategorieDao extends Dao<Kategorie, Long>{
	/**
	 * Liefert alle Kategorien ein Benutzers.
	 * @param benutzer Benutzer (Pflicht).
	 * @return Liste mit allen Kategorien, die der Benutzer angelegt hat.
	 * @throws SQLException
	 */
	public abstract List<Kategorie> getAllKategorienByBenutzer(Benutzer benutzer) throws SQLException;
}
