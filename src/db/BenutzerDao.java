package db;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;

import model.Benutzer;
/**
 * DAO der Fachklasse Benutzer.
 */
public interface BenutzerDao extends Dao<Benutzer, Long>{
	
	/**
	 * Liefert den Benutzer falls die Kombination aus EMail und Passwort ind der DB existiert.
	 * @param email EMail (Pflicht).
	 * @param password	Passwort (Pflicht).
	 * @return Benutzerobjekt falls die Kombination aus EMail und Passwort in der DB existiert, sonst null;
	 * @throws SQLException
	 */
	public abstract Benutzer getBenutzer(String email, String password) throws SQLException;
}
