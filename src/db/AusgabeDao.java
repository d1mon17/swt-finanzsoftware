package db;

import com.j256.ormlite.dao.Dao;

import model.Ausgabe;
/**
 * DAO der Fachklasse Ausgabe.
 */
public interface AusgabeDao extends Dao<Ausgabe, Long> {
	// Kann hier um sinnvolle Methoden erweitert werden.
}
