package db;

import java.sql.SQLException;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import model.Benutzer;

/**
 * Implementation vom BenutzerDAO.
 */
public class BenutzerDaoImpl extends BaseDaoImpl<Benutzer, Long> implements BenutzerDao {
	
	/**
	 * Konstruktor der Klasse BenutzerDaoImpl.
	 * @param connectionSource Datenbankverbindung (Pflicht).
	 * @throws SQLException
	 */
	public BenutzerDaoImpl(ConnectionSource connectionSource) throws SQLException {
		super(connectionSource, Benutzer.class);
	}
	
	@Override
	public Benutzer getBenutzer(String email, String password) throws SQLException {
		return queryBuilder().where()
				.eq("email", email).and().eq("password", password)
				.queryForFirst();
	}
}
