package db;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import model.Bankkonto;
import model.Benutzer;

/**
 * Implementation vom BankkontoDaoImpl.
 */
public class BankkontoDaoImpl extends BaseDaoImpl<Bankkonto, Long> implements BankkontoDao {
	/**
	 * Konstruktor der Klasse BankkontoDaoImpl.
	 * @param connectionSource Datenbankverbindung (Pflicht).
	 * @throws SQLException
	 */
	public BankkontoDaoImpl(ConnectionSource connectionSource) throws SQLException {
		super(connectionSource, Bankkonto.class);
	}

	@Override
	public List<Bankkonto> getAllBankkontenByBenutzer(Benutzer benutzer) throws SQLException {
		return queryForEq("benutzer_id", benutzer.getId()); // fremdschlüssel benutzer_id
	}

	@Override
	public Bankkonto getBankkontoByBezeichnung(String bezeichnung) throws SQLException {
		return queryBuilder().where()
				.eq("bezeichnung", bezeichnung)
				.queryForFirst();
	}
}