package db;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.Dao;

import model.Bankkonto;
import model.Benutzer;

/**
 * DAO der Fachklasse Bankkonto.
 */
public interface BankkontoDao extends Dao<Bankkonto, Long>{
	/**
	 * L�dt alle Bankkonten des Benutzers aus der Datenbank.
	 * @param benutzer Benutzer (Pflicht).
	 * @return Bankkonten des Benutzers.
	 * @throws SQLException
	 */
	public abstract List<Bankkonto> getAllBankkontenByBenutzer(Benutzer benutzer) throws SQLException;
	
	/**
	 * Liefert ein Bankkonto, falls in der Datenbank vorhanden.
	 * @param bezeichnung Bezeichnung vom Benutzerkonto (Pflicht).
	 * @return Bankkonto falls in der Datenbank vorhanden, sonst null.
	 * @throws SQLException
	 */
	public abstract Bankkonto getBankkontoByBezeichnung(String bezeichnung) throws SQLException;
}
