package db;

import com.j256.ormlite.dao.Dao;

import model.Einnahme;

/**
 * DAO der Fachklasse Einnahme.
 */
public interface EinnahmeDao extends Dao<Einnahme, Long> {
	// Kann hier um sinnvolle Methoden erweitert werden.
}
