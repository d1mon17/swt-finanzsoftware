package db;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import model.Bankkonto;
import model.Transaktion;

/**
 *	DAO der Fachklasse Transaktion.
 */
public interface TransaktionDao{
	
	/**
	 * Liefert alle Transaktionen von einem Bankkonto.<br>
	 * Liste mit Transaktionen im Bankkonto wird automatisch aktualisiert.
	 * @param bankkonto Bankkonto (Pflicht).
	 * @return	Liste mit Transaktionen.
	 * @throws SQLException
	 */
	public List<Transaktion> getAllTransaktionenByKonto(Bankkonto bankkonto) throws SQLException;
	
	/**
	 * F�gt eine Transaktion der Datenbank hinzu.<br>
	 * Liste mit Transaktionen im Bankkonto wird automatisch aktualisiert.
	 * @param transaktion Transaktion (Pflicht).
	 * @throws SQLException
	 */
	public void createTransaktion(Transaktion transaktion) throws SQLException;
	
	/**
	 * Siehe createTransaktion(Transaktion transaktion).
	 * @param transaktionen Transaktionen (Pflicht).
	 * @throws SQLException
	 */
	public void createTransaktionen(Collection<Transaktion> transaktionen) throws SQLException;
	/**
	 * Entfernt eine Transaktion aus der Datenbank.<br>
	 * Liste mit Transaktionen im Bankkonto wird automatisch aktualisiert.
	 * @param transaktion Transaktion (Pflicht).
	 * @throws SQLException
	 */
	public void removeTransaktion(Transaktion transaktion) throws SQLException;
}
