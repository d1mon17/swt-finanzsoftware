package controller;

import java.sql.SQLException;

import db.BankkontoDao;
import db.Datenbank;
import model.Bankkonto;
import model.Benutzer;
import model.builders.BankkontoBuilder;
import view.FinanzSoftware;

/**
 * Klasse KontoManager
 */

public class KontoManager {
	private Datenbank db;
	private BankkontoDao bankkontoDao;
	private Benutzer benutzer;
	private Bankkonto bankkonto;
	private FinanzSoftware viewFinanzSoftware;

	/**
	 * Konstruktor f�r die Klasse KontoManager
	 * @param viewFinanzsoftware	die View, mit der gearbeitet wird
	 * @param benutzer	der aktuelle Benutzer
	 * @param db	die aktuelle Datenbank
	 * @param bk	das aktuelle Bankkonto
	 */
	public KontoManager(FinanzSoftware viewFinanzsoftware, Benutzer benutzer, Datenbank db, Bankkonto bk) {
		this.viewFinanzSoftware = viewFinanzsoftware;
		this.benutzer = benutzer;
		this.db = db;
		this.bankkontoDao = db.getBankkontoDao();
		this.bankkonto = bk;
	}

	/**
	 * F�gt ein neues Bankkonto in die Datenbank hinzu
	 * @param bez	Bezeichnung
	 * @param iban	IBAN-Nummer
	 * @param bic	BIC-Nummer
	 * @param bname	Name der Bank
	 * @param kontostand Kontostand
	 * @return true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean addBankkonto(String bez, String iban, String bic, String bname, double kontostand)
			throws SQLException {
		Bankkonto account = new Bankkonto(this.benutzer, bez, kontostand, iban, bic, bname);
		bankkontoDao.create(account);
		return true;
	}
	
	/**
	 * Holt ein existierendes Bankkonto aus der Datenbank nach Bezeichnung
	 * @param bez	Die Bezeichnung, womit das gew�nschte Bankkonto gesucht werden soll
	 * @return		Bankkonto nach Bezeichnung
	 * @throws SQLException
	 */
	public Bankkonto getBankkonto(String bez) throws SQLException {
		return bankkontoDao.getBankkontoByBezeichnung(bez);
	}
	
	/**
	 * Entfernt ein existierendes Bankkonto aus der Datenbank nach Bezeichnung
	 * @param bez	Die Bezeichnung, womit das gew�nschte Bankkonto gesucht werden soll
	 * @return	true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean removeBankkonto(String bez) throws SQLException {
		bankkontoDao.delete(bankkontoDao.getBankkontoByBezeichnung(bez));
		return true;
	}
	/**
	 * Gibt den tats�chlichen aktuellen Kontostand eines Bankkontos zur�ck
	 * @return	der aktuelle (reale) Kontostand
	 */
	public double getRealKontostand() {
		return this.bankkonto.getKontostand();
	}
	
	/**
	 * Berechnet den Kontostand nach dem Totalwert.
	 * @return	der aktuelle Kontostand nach Totalwert
	 */
	public double getBerechnetKontostand() {
		return this.bankkonto.calcTotalWert();
	}

}
