package controller;

import java.sql.SQLException;
import java.util.*;

import db.BankkontoDao;
import db.Datenbank;
import db.KategorieDao;
import db.TransaktionDao;
import model.*;
import view.FinanzSoftware;
import view.NeueTransaktion;

/**
 * Klasse TransaktionManager
 */
public class TransaktionManager {
	private Datenbank db;
	private TransaktionDao transaktionDao;
	private BankkontoDao bankkontoDao;
	private Bankkonto bankkonto;
	private Benutzer benutzer;
	private Transaktion transaktion;
	private FinanzSoftware viewFinanzSoftware;
	private NeueTransaktion viewNeueAusgabe;
	private KategorieDao kategorieDao;

	/**
	 * Konstruktor der Klasse TransaktionManager
	 * @param viewFinanzsoftware	die View, mit der gearbeitet wird
	 * @param benutzer	der aktuelle Benutzer
	 * @param db		die aktuelle Datenbank
	 * @param bankkonto	das aktuelle Bankkonto
	 * @throws SQLException
	 */
	public TransaktionManager(FinanzSoftware viewFinanzsoftware, Benutzer benutzer, Datenbank db, Bankkonto bankkonto)
			throws SQLException {
		this.viewFinanzSoftware = viewFinanzsoftware;
		this.benutzer = benutzer;
		this.db = db;
		this.bankkonto = bankkonto;
		transaktionDao = db.getTransaktionDao();
		this.transaktionDao.getAllTransaktionenByKonto(bankkonto);
	}

	/**
	 * Aktualisiert den Kontostand des aktuellen Bankkontos
	 * @param wert	Wert, nach dem der Kontostand aktualisiert wird
	 * @return	true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean updateKontostand(double wert) throws SQLException {
		Bankkonto bk = this.bankkonto;
		bk.setKontostand(wert);
		bankkontoDao.update(bk);
		return false;
	}

	/**
	 * Holt alle Transaktionen des aktuellen Bankkontos und gibt sie als Liste zur�ck
	 * @return	Liste von Transaktionen
	 */
	public List<Transaktion> getTransaktionen() {
		return this.bankkonto.getTransaktionen();
	}
	
	/**
	 * Filtert Transaktionen nach bestimmten Kriterien
	 * @param transaktionTyp Art der Transaktion
	 * @param bez	Bezeichnung
	 * @param ktg	Kategorie
	 * @param startDatum	Beginn der Zeitspanne
	 * @param endDatum		Ende der Zeitspanne
	 * @return	Liste von gefilterten Transaktionen
	 */
	public List<Transaktion> getFilterTransaktion(String transaktionTyp, String bez, Kategorie ktg, String startDatum,
			String endDatum) {
		return this.bankkonto.getFilterTransaktion(transaktionTyp, bez, ktg, startDatum, endDatum);
	}

	/**
	 * Entfernt eine Kategorie nach einem Schl�ssel (key)
	 * @param key Schl�ssel
	 * @return true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean removeKategorie(int key) throws SQLException {
		List<Kategorie> ls = new ArrayList<>(this.benutzer.getKategorien());
		Kategorie kateg = ls.get(key);
		kategorieDao.delete(kateg);
		return true;
	}

	/**
	 * Bearbeitet eine existierende Transaktion und deren Kriterien
	 * @param id	ID
	 * @param wrng	W�hrung
	 * @param bez	Bezeichnung
	 * @param bes	Beschreibung
	 * @param dt	Datum
	 * @param ktg	Kategorie
	 * @param intv	Art des Intervalls
	 * @param zeits	Zeitspanne
	 * @param abs	Absender, falls bei der Transaktion um eine Einnahme handelt
	 * @param empf	Empf�nger, falls bei der Transaktion um eine Ausgabe handelt
	 * @return	true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean editTransaktion(long id, String wrng, String bez, String bes, String dt, Kategorie ktg, int intv,
			String zeits, String abs, String empf) throws SQLException {
		List<Transaktion> tl = db.getTransaktionDao().getAllTransaktionenByKonto(bankkonto);
		Transaktion t = tl.get((int) id);
		transaktionDao.removeTransaktion(t);
		if (t instanceof Einnahme) {
			t = new Einnahme(ktg, this.bankkonto, this.bankkonto.getKontostand(), wrng, bez, bes, dt, intv, zeits, abs);
			transaktionDao.createTransaktion(t);
			return true;
		} else if (t instanceof Ausgabe) {
			t = new Ausgabe(ktg, this.bankkonto, this.bankkonto.getKontostand(), wrng, bez, bes, dt, intv, zeits, empf);
			transaktionDao.createTransaktion(t);
			return true;
		}
		return false;
	}

	/**
	 * F�gt eine neue Transaktion hinzu
	 * @param typ	Art der Transaktion
	 * @param wrt	Wert
	 * @param wrng	W�hrung
	 * @param bez	Bezeichnung
	 * @param bes	Beschreibung
	 * @param dt	Datum
	 * @param ktg	Kategorie
	 * @param intv	Art des Intervalls
	 * @param zeits	Zeitspanne
	 * @param abs	Absender, falls bei der Transaktion um eine Einnahme handelt
	 * @param empf	Empf�nger, falls bei der Transaktion um eine Ausgabe handelt
	 * @return true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean addTransaktion(String typ, double wrt, String wrng, String bez, String bes, String dt, Kategorie ktg,
			int intv, String zeits, String abs, String empf) throws SQLException {
		Transaktion t;
		if (typ.equals("Einnahme")) {
			t = new Einnahme(ktg, this.bankkonto, wrt, wrng, bez, bes, dt, intv, zeits, abs);
			transaktionDao.createTransaktion(t);
			return true;
		} else if (typ.equals("Ausgabe")) {
			t = new Ausgabe(ktg, this.bankkonto, wrt, wrng, bez, bes, dt, intv, zeits, empf);
			transaktionDao.createTransaktion(t);
			return true;
		}
		return false;
	}

	/**
	 * Gleicht Differenz auf dem aktuellen Bankkonto aus
	 * @return 
	 */
	public double diff() {
		return this.bankkonto.differenzAusgleichen();
	}

	/**
	 * Berechnet den Totalwert des aktuellen Bankkontos
	 * @return
	 */
	public double totalWert() {
		return this.bankkonto.calcTotalWert();
	}

}
