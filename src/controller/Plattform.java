package controller;

import java.sql.SQLException;
import java.util.List;

import db.BenutzerDao;
import db.Datenbank;
import db.KategorieDao;
import model.*;
import model.builders.BenutzerBuilder;
import view.FinanzSoftware;

/**
 * Klasse Plattform
 */

public class Plattform {
	private Datenbank db;
	private Benutzer benutzer;
	private BenutzerDao benutzerDao;
	private KategorieDao kategorieDao;
	private FinanzSoftware viewFinanzSoftware;
	public KontoManager kontoManager;
	public TransaktionManager transaktionManager;

	/**
	 * Konstruktor der Klasse Plattform
	 * @param viewFinanzSoftware	die View, mit der gearbeitet wird
	 * @param db					die aktuelle Datenbank
	 */
	public Plattform(FinanzSoftware viewFinanzSoftware, Datenbank db) {
		this.viewFinanzSoftware = viewFinanzSoftware;
		this.db = db;
		benutzerDao = db.getBenutzerDao();
		kategorieDao = db.getKategorieDao();
	}

	/**
	 * Loggt den Benutzer mit Angabe seiner E-Mail-Adresse und seines Passworts ein
	 * @param email		Die E-Mail-Adresse
	 * @param passwort	Das Passwort
	 * @return	true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean einloggen(String email, String passwort) throws SQLException {
		try {
			benutzer = benutzerDao.getBenutzer(email, passwort);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (benutzer != null) {
			Bankkonto bk = db.getBankkontoDao().getAllBankkontenByBenutzer(benutzer).get(0);
			kontoManager = new KontoManager(viewFinanzSoftware, benutzer, db, bk);
			transaktionManager = new TransaktionManager(viewFinanzSoftware, benutzer, db, bk);
			return true;
		}
		return false;
	}

	/**
	 * Loggt den aktuellen Benutzer aus
	 * @return true, wenn die Aktion erfolgreich war
	 */
	public boolean ausloggen() {
		if (benutzer != null && kontoManager != null && transaktionManager != null) {
			benutzer = null;
			kontoManager = null;
			transaktionManager = null;
			return true;
		}
		return false;
	}

	/**
	 * Legt einen neuen Benutzer an anhand von bestimmten Daten
	 * @param email		Die E-Mail-Adresse
	 * @param passwort	Das Passwort
	 * @param nachname	Nachname des neuen Benutzers
	 * @param vorname	Vorname des neuen Benutzers
	 * @param gebDatum	Geburtsdatum
	 * @param strasse	Strasssnbezeichnung
	 * @param hNr		Hausnummer
	 * @param plz		Postleitzahl
	 * @param ort		Ort
	 * @param land		Land
	 * @return	true, wenn die Aktion erfolgreich war
	 */
	public boolean registrieren(String email, String passwort, String nachname, String vorname, String gebDatum, String strasse, String hNr,
			int plz, String ort, String land) {
		benutzer = new BenutzerBuilder(email, passwort)
				.nachname(nachname)
				.vorname(vorname)
				.gebDatum(gebDatum)
				.strasse(strasse)
				.hausNr(hNr)
				.plz(plz)
				.ort(ort)
				.land(land)
				.build();
		try {
			benutzerDao.create(benutzer);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * F�gt dem Benutzer eine Kategorie hinzu
	 * @param bez Bezeichnung
	 * @return	true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean addKategorie(String bez) throws SQLException {
		Kategorie kateg = new Kategorie(this.benutzer, bez);
		kategorieDao.create(kateg);
		return true;
	}

	/**
	 * Entfernt eine Kategorie anhand einer Bezeichnung
	 * @param bez Bezeichnung
	 * @return	true, wenn die Aktion erfolgreich war
	 * @throws SQLException
	 */
	public boolean removeKategorie(String bez) throws SQLException {
		Kategorie kateg = new Kategorie(this.benutzer, bez);
		kategorieDao.delete(kateg);
		return true;
	}
	
	/**
	 * Holt alle Kategorien eines Benutzer von der Datenbank und legt sie als Liste von Kategorien an
	 * @return	Liste vom Kategorien
	 * @throws SQLException
	 */
	public List<Kategorie> getKategorien() throws SQLException{
		return kategorieDao.getAllKategorienByBenutzer(benutzer);
	}
}
