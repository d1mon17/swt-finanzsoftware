package model.builders;

import model.Bankkonto;
import model.Benutzer;

/**
 * Builder zum Erstellen eines Bankkontos
 */
public class BankkontoBuilder {
	/**
	 * Der Benutzer, dem das Bankkonto gehoehrt
	 */
	private Benutzer benutzer;
	
	/**
	 * Bezeichnung des Kontos
	 */
	private String bezeichnung;
	
	/**
	 * Kontostand des Bankkontos
	 */
	private double kontostand;
	
	/**
	 * Iban des Bankkontos
	 */
	private String iban = null;
	
	/**
	 * BIC des Bankkontos
	 */
	private String bic = null;
	
	/**
	 * Name der Bank bei der das Konto ist
	 */
	private String bankName = null;
	
	/**
	 * Erstellt einen BankkontoBuilder mit den noetigen Pflichtfeldern eines Bankkontos
	 * @param benutzer Benutzer des Kontos
	 * @param bezeichnung Bezeichnung des Kontos
	 * @param kontostand Kontostand des Kontos
	 */
	public BankkontoBuilder(Benutzer benutzer, String bezeichnung, double kontostand) {
		this.bezeichnung = bezeichnung;
		this.kontostand = kontostand;
		this.benutzer = benutzer;
	}
	
	/**
	 * Erstellt ein Bankkonto aus einem BankkontoBuilder
	 * @return Ein Bankkonto, welches die Attribute des BankkontoBuilders besitzt
	 */
	public Bankkonto build() {
		return new Bankkonto(this);
	}
	
	/**
	 * Setzt die Iban fuer einen BankkontoBuilder
	 * @param iban Die Iban, welche fuer den BankkontoBuilder gesetzt werden soll
	 * @return Den mit der Iban erweiterten BankkontoBuilder
	 */
	public BankkontoBuilder iban (String iban) {
		this.iban = iban;
		return this;
	}
	
	/**
	 * Setzt den BIC fuer einen BankkontoBuilder
	 * @param bic Der BIC , welcher fuer den BankkontoBuilder gesetzt werden soll
	 * @return Den mit dem BIC erweiterten BankkontoBuilder
	 */
	public BankkontoBuilder bic(String bic) {
		this.bic = bic;
		return this;
	}
	
	/**
	 * Setzt den Banknamen fuer einen BankkontoBuidler
	 * @param bankname Der Bankname, welcher fuer den BankkontoBuilder gesetzt werden soll
	 * @return Den mit dem Banknamen erweiterten BankkontoBuilder
	 */
	public BankkontoBuilder bankname(String bankname) {
		this.bankName = bankname;
		return this;
	}

	/**
	 * Gibt den Benutzer des BankkontoBuilders zurueck
	 * @return Der Benutzer, welcher im BankkontoBuidler gesetzt ist
	 */
	public Benutzer getBenutzer() {
		return benutzer;
	}
	
	/**
	 * Gibt die Bezeichnung des BankkontoBuilders zurueck
	 * @return Die Bezeichnung, welche im BankontoBuilder gesetzt ist
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * Gibt die IBAN des BankkontoBuilders zurueck
	 * @return Die IBAN, welche im BankkontoBuilder gesetzt ist
	 */
	public String getIban() {
		return iban;
	}

	/**
	 * Gibt den BIC des BankkontoBuilders zurueck
	 * @return Der BIC, welcher im BankkontoBuilder gesetzt ist
	 */
	public String getBic() {
		return bic;
	}

	/**
	 * Gibt den Banknamen des BankkontoBuilders zurueck
	 * @return Der Bankname, welcher im BankkontoBuilder gesetzt ist 
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * Gibt den Kontostand des BankkontoBuilders zurueck
	 * @return Der Kontostand, welcher im BankkontoBuilder gesetzt ist
	 */
	public double getKontostand() {
		return kontostand;
	}
}