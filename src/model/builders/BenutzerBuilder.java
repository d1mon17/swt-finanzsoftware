package model.builders;

import model.Benutzer;

/**
 * Builder zum Erstellen eines Benutzers
 */
public class BenutzerBuilder {
	
	/**
	 * Email des Benutzers
	 */
	private String email;
	
	/**
	 * Passwort des Benutzers
	 */
	private String passwort;
	
	/**
	 * Nachname des Benutzers
	 */
	private String nachname = null;
	
	/**
	 * Vorname des Benutzers
	 */
	private String vorname = null;
	
	/**
	 * Geburtsdatum des Benutzers
	 */
	private String gebDatum = null;
	
	/**
	 * Strasse der Wohnadresse des Benuzters
	 */
	private String strasse = null;
	
	/**
	 * Hausnummer des Benutzers
	 */
	private String hausNr = null;
	
	/**
	 * Postleitzahl des Benutzers
	 */
	private int plz = -1;
	
	/**
	 * Ort des Wohnsitzes des Benutzers
	 */
	private String ort = null;
	
	/**
	 * Land des Wohnsitzes des Benutzers
	 */
	private String land = null;
	
	/**
	 * Erstellt einen BenutzerBuider mit den noetigen Pflichtfeldern eines Benutzers
	 * @param email Die EMail des Benutzers
	 * @param passwort Das PassWort des Benutzers
	 */
	public BenutzerBuilder(String email, String passwort) {
		this.email = email;
		this.passwort = passwort;
	}
	
	/**
	 * Erstellt einen Benutzer aus dem BenutzerBuilder
	 * @return Ein Benutzer mit den Attributwerten des BenutzerBuilders
	 */
	public Benutzer build() {
		return new Benutzer(this);
	}

	/**
	 * Gibt die Email des BenutzerBuilders zurueck
	 * @return Die Email, welche im BenutzerBuilder gesetzt ist
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gibt das Passwort des BenutzerBuilders zurueck
	 * @return Das Passwort, welches im BenutzerBuilder gesetzt ist
	 */
	public String getPasswort() {
		return passwort;
	}
	/**
	 * Gibt den Nachnamen des BenutzerBuilders zurueck
	 * @return Der Nachname, welcher im BenutzerBuilder gesetzt ist
	 */
	public String getNachname() {
		return nachname;
	}
	/**
	 * Gibt den Vornamen des BenutzerBuilders zurueck
	 * @return Der Vorname, welcher im BenutzerBuilder gesetzt ist
	 */
	public String getVorname() {
		return vorname;
	}
	/**
	 * Gibt das Geburtsdatum des BenutzerBuilders zurueck
	 * @return Das Geburtsdatum, welches im BenutzerBuilder gesetzt ist
	 */
	public String getGebDatum() {
		return gebDatum;
	}
	/**
	 * Gibt die Strasse des BenutzerBuilders zurueck
	 * @return Die Strasse, welche im BenutzerBuilder gesetzt ist
	 */
	public String getStrasse() {
		return strasse;
	}
	/**
	 * Gibt die Hausnummer des BenutzerBuilders zurueck
	 * @return Die Hausnummer, welche im BenutzerBuilder gesetzt ist
	 */
	public String getHausNr() {
		return hausNr;
	}
	/**
	 * Gibt die Postleitzahl des BenutzerBuilders zurueck
	 * @return Die Postleitzahl, welche im BenutzerBuilder gesetzt ist
	 */
	public int getPlz() {
		return plz;
	}
	/**
	 * Gibt den Ort des BenutzerBuilders zurueck
	 * @return Der Ort, welcher im BenutzerBuilder gesetzt ist
	 */
	public String getOrt() {
		return ort;
	}
	/**
	 * Gibt das Land des BenutzerBuilders zurueck
	 * @return Das Land, welches im BenutzerBuilder gesetzt ist
	 */
	public String getLand() {
		return land;
	}

	/**
	 * Setzt den Nachnamen f�r ein BenutzerBuilder
	 * @param nachname Der Nachname, welcher fuer den BenutzerBuilder gesetzt werden soll
	 * @return Den mit dem Nachnamen erweiterten BenutzerBuilder
	 */
	public BenutzerBuilder nachname(String nachname) {
		this.nachname = nachname;
		return this;
	}

	/**
	 * Setzt den Vornamen f�r ein BenutzerBuilder
	 * @param vorname Der Vorname, welcher fuer den BenutzerBuilder gesetzt werden soll
	 * @return Den mit dem Vornamen erweiterten BenutzerBuilder
	 */
	public BenutzerBuilder vorname(String vorname) {
		this.vorname = vorname;
		return this;
	}

	/**
	 * Setzt das Geburtsdatum f�r ein BenutzerBuilder
	 * @param gebDatum Das Geburtsdatum, welches fuer den BenutzerBuilder gesetzt werden soll
	 * @return Den mit dem Geburtsdatum erweiterten BenutzerBuilder
	 */
	public BenutzerBuilder gebDatum(String gebDatum) {
		this.gebDatum = gebDatum;
		return this;
	}

	/**
	 * Setzt die Strasse f�r ein BenutzerBuilder
	 * @param strasse Die Strasse, welche fuer den BenutzerBuilder gesetzt werden soll
	 * @return Den mit der Strasse erweiterten BenutzerBuilder
	 */
	public BenutzerBuilder strasse(String strasse) {
		this.strasse = strasse;
		return this;
	}

	/**
	 * Setzt die Hausnummer f�r ein BenutzerBuilder
	 * @param hausNr Die Hausnummer, welche fuer den BenutzerBuilder gesetzt werden soll
	 * @return Den mit der Hausnummer erweiterten BenutzerBuilder
	 */
	public BenutzerBuilder hausNr(String hausNr) {
		this.hausNr = hausNr;
		return this;
	}

	/**
	 * Setzt die Postleitzahl f�r ein BenutzerBuilder
	 * @param plz Die Postleitzahl, welche fuer den BenutzerBuilder gesetzt werden soll
	 * @return Den mit der Postleitzahl erweiterten BenutzerBuilder
	 */
	public BenutzerBuilder plz(int plz) {
		this.plz = plz;
		return this;
	}
	
	/**
	 * Setzt den Ort f�r ein BenutzerBuilder
	 * @param ort Der Ort, welcher fuer den BenutzerBuilder gesetzt werden soll
	 * @return Den mit dem Ort erweiterten BenutzerBuilder
	 */
	public BenutzerBuilder ort(String ort) {
		this.ort = ort;
		return this;
	}

	/**
	 * Setzt das Land f�r ein BenutzerBuilder
	 * @param land Das Land, welches fuer den BenutzerBuilder gesetzt werden soll
	 * @return Den mit dem Land erweiterten BenutzerBuilder
	 */
	public BenutzerBuilder land(String land) {
		this.land = land;
		return this;
	}
	
	
}
