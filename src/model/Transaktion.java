package model;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.j256.ormlite.field.DatabaseField;

/**
 * Fachklasse Transaktion.
 */
public abstract class Transaktion {
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	@DatabaseField(generatedId = true)
	private long id;
	@DatabaseField(canBeNull = false)
	private double wert;
	@DatabaseField(canBeNull = false)
	private String waehrung;
	@DatabaseField
	private String bezeichnung;
	@DatabaseField
	private String beschreibung;
	@DatabaseField
	private String datum;
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Kategorie kategorie;
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Bankkonto bankkonto;
	@DatabaseField
	private int intervall;
	@DatabaseField
	private String zeitspanne;
	
	/**
	 * Nicht benutzen.<br>
	 * Leerer Konstruktor f�r ORM Lite.
	 */
	protected Transaktion() {/*Leerer Konstruktor f�r ORM Lite.*/}
	
	/**
	 * Konstroktor der Klasse Transaktion.
	 * @param bankkonto Bankkonto dem die Transaktion geh�rt (Pflicht).
	 * @param kategorie Kategorie der Transaktion (Pflicht).
	 * @param wert Wert der Transaktion (Pflicht).
	 * @param waehrung Waehrung (Pflicht).
	 * @param bezeichnung Bezeichnung (optional).
	 * @param beschreibung 	Beschreibung (optional).
	 * @param datum Datum an den die Transaktion gelaufen ist (optional).
	 * @param intervall Intervall f�r Regelmaessigkeit (optional).
	 * @param zeitspanne Zeitspanne (optional).
	 */
	protected Transaktion(Bankkonto bankkonto, Kategorie kategorie, double wert, String waehrung, String bezeichnung,
			String beschreibung, String datum, int intervall, String zeitspanne) {
		this.bankkonto = bankkonto;
		this.kategorie = kategorie;
		this.wert = wert;
		this.waehrung = waehrung;
		this.bezeichnung = bezeichnung;
		this.beschreibung = beschreibung;
		this.datum = datum.isEmpty() ? dateFormat.format(new Date()) : datum; // Falls kein Datum vorhanden, setze aktuelles Datum.
		this.intervall = intervall;
		this.zeitspanne = zeitspanne;
	}

	public double getWert() {
		return wert;
	}

	public void setWert(double wert) {
		this.wert = wert;
	}

	public String getWaehrung() {
		return waehrung;
	}

	public void setWaerung(String waehrung) {
		this.waehrung = waehrung;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public Kategorie getKategorie() {
		return kategorie;
	}

	public void setKategorie(Kategorie kategorie) {
		this.kategorie = kategorie;
	}

	public int getIntervall() {
		return intervall;
	}

	public void setIntervall(int intervall) {
		this.intervall = intervall;
	}

	public String getZeitspanne() {
		return zeitspanne;
	}

	public void setZeitspanne(String zeitspanne) {
		this.zeitspanne = zeitspanne;
	}

	public long getId() {
		return id;
	}

	public Bankkonto getBankkonto() {
		return bankkonto;
	}

	public void setBankkonto(Bankkonto bankkonto) {
		this.bankkonto = bankkonto;
	}
	@Override
	public boolean equals(Object obj) {
		Transaktion other = (Transaktion) obj;
		return (this instanceof Einnahme && other instanceof Einnahme ||
				this instanceof Ausgabe && other instanceof Ausgabe) && this.getId() == other.getId();
				
	}
}
