package model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import db.BankkontoDaoImpl;
import model.builders.BankkontoBuilder;

/**
 * Fachklasse Bankkonto.
 */
@DatabaseTable(daoClass = BankkontoDaoImpl.class, tableName = "bankkonto")
public class Bankkonto {
	@DatabaseField(generatedId = true)
	private long id;
	@DatabaseField(canBeNull = false, unique = true)
	private String bezeichnung;
	@DatabaseField
	private String iban;
	@DatabaseField
	private String bic;
	@DatabaseField
	private String bankName;
	@DatabaseField(canBeNull = false)
	private double kontostand;
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Benutzer benutzer;
	private List<Transaktion> transaktionen = new ArrayList<>();


	/**
	 * Nicht benutzen.<br>
	 * Leerer Konstruktor f�r ORM Lite.
	 */
	Bankkonto() {/*Leerer Konstruktor f�r ORM Lite.*/}
	/**
	 * Konstruktor der Klasse Bankkonto.
	 * @param benutzer Benutzer des Bankkontos (Pflicht).
	 * @param bezeichnung Bezeichnung (Pflicht).
	 * @param kontostand Kontostand (Pflicht).
	 * @param iban IBAN (optional).
	 * @param bic BIC (optional).
	 * @param bankName Name der Bank (optional).
	 */
	public Bankkonto(Benutzer benutzer, String bezeichnung, double kontostand, String iban, String bic, String bankName) {
		this.benutzer = benutzer;
		this.bezeichnung = bezeichnung;
		this.kontostand = kontostand;
		this.iban = iban;
		this.bic = bankName;
		this.bankName = bankName;
	}
	
	public Bankkonto(BankkontoBuilder bankkontoBuilder) {
		this.bezeichnung = bankkontoBuilder.getBezeichnung();
		this.kontostand = bankkontoBuilder.getKontostand();
		this.iban = bankkontoBuilder.getIban();
		this.bic = bankkontoBuilder.getBic();
		this.bankName = bankkontoBuilder.getBankName();
		this.benutzer = bankkontoBuilder.getBenutzer();
	}
	
	public Benutzer getBenutzer() {
		return benutzer;
	}



	public void setBenutzer(Benutzer benutzer) {
		this.benutzer = benutzer;
	}
	
	/**
	 * Berechnet den Gesamtwert f�r den Kontostand (nicht realer Kontostand).<br>
	 * Um als realen Kontostand zu setzen, nutze differenzAusgleichen().
	 * @return errechneter (nicht realen) Kontostand.
	 */
	public double calcTotalWert() {
		return getKontostand() + transaktionen.stream()
				.map(t -> t instanceof Einnahme ? t.getWert() : -t.getWert()) // falls Einnahme +Wert, sonst -Wert.
				.reduce(0.0, (t1, t2) -> t1 + t2);
	}
	
	/**
	 * Setze den Errechneten Wert als neuen Kontostand.
	 * @return neuer Kontostand.
	 */
	public double differenzAusgleichen() {
		kontostand = calcTotalWert();
		return kontostand;
	}
	
	/**
	 * Liefert eine gefilterte Liste mit Transaktionen.<br>
	 * Falls bei den Paramtern transaktionTyp, bezeichnung, startDatum oder endDatum keine
	 * Eingabe erfolgen soll muss statt null ein leerer String "" �bergeben werden.<br>
	 * Startdatum und Enddatum m�ssen entweder beide leer oder nichr leer sein.
	 * @param transaktionTyp Typ der Transaktion (Einnahme/Ausgabe) (optional).
	 * @param bezeichnung Bezeichnung der Transaktion (optional).
	 * @param kategorie Kategorie der Transaktion (optional).
	 * @param startDatum Datum im Format: tt.MM.jjjj (optional oder Pflicht falls endDatum vorhanden). 
	 * @param endDatum Datum im Format: tt.MM.jjjj (optional oder Pflicht falls startDatum vorhanden).
	 * @return gefilterte Liste mit Transaktionen.
	 */
	
	public List<Transaktion> getFilterTransaktion(String transaktionTyp, String bezeichnung, Kategorie kategorie, String startDatum, String endDatum) {
		return transaktionen.stream()
				.filter(t -> transaktionTyp.isEmpty() || t.getClass().getSimpleName().matches(transaktionTyp))
				.filter(t -> bezeichnung.isEmpty() || t.getBezeichnung().matches(bezeichnung))
				.filter(t -> (kategorie == null) || t.getKategorie().equals(kategorie))
				.filter(t -> (startDatum.isEmpty() && endDatum.isEmpty()) || isDateInRange(startDatum, endDatum, t.getDatum()))
				.collect(Collectors.toList());
	}
	
	/**
	 * Pr�ft ob ein Datum zwischen zwei Daten liegt.<br>
	 * Daten m�ssen in diesem Format angegeben werden: tt.MM.jjjj.
	 * @param startDatum Startfdatum (Pflicht).
	 * @param endDatum Enddatum (Pflicht).
	 * @param datum Datum welches gepr�ft werden soll (Pflicht).
	 * @return true falls datum zwischen startDatum und endDatum liegt, false anderfalls.
	 */
	private boolean isDateInRange(String startDatum, String endDatum, String datum) {
		try {
			SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
			Date start = format.parse(startDatum);
			Date end = format.parse(endDatum);
			Date current = format.parse(datum);
			return !(current.before(start) || current.after(end));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
		
	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public double getKontostand() {
		return kontostand;
	}

	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}

	public long getId() {
		return id;
	}
	public List<Transaktion> getTransaktionen() {
		return transaktionen;
	}
	public void setTransaktionen(List<Transaktion> transaktionen) {
		this.transaktionen = transaktionen;
	}
	@Override
	public boolean equals(Object obj) {
		Bankkonto other = (Bankkonto)obj;
		return this.getBezeichnung().matches(other.getBezeichnung());
	}
	
}
