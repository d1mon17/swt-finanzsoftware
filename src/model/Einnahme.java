package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import db.EinnahmeDaoImpl;

/**
 * Fachklasse Einnahme, Unterklasse von Transaktion.
 */
@DatabaseTable(daoClass = EinnahmeDaoImpl.class, tableName = "einnahme")
public class Einnahme extends Transaktion {
	
	@DatabaseField
	private String absender;
	
	/**
	 * Nicht benutzen.<br>
	 * Leerer Konstruktor f�r ORM Lite.
	 */
	public Einnahme() {/*Leerer Konstruktor f�r ORM Lite.*/}
	/**
	 * Konstroktor der Klasse Einnahme.
	 * @param bankkonto Bankkonto dem die Transaktion(Einnahme) geh�rt (Pflicht).
	 * @param kategorie Kategorie der Transaktion(Einnahme) (Pflicht).
	 * @param wert Wert der Transaktion(Einnahme) (Pflicht).
	 * @param waehrung Waehrung (Pflicht).
	 * @param bezeichnung Bezeichnung (optional).
	 * @param beschreibung 	Beschreibung (optional).
	 * @param datum Datum an den die Transaktion(Einnahme) gelaufen ist (optional).
	 * @param intervall Intervall f�r Regelmaessigkeit (optional).
	 * @param zeitspanne Zeitspanne (optional).
	 * @param absender Absender (optional).
	 */
	public Einnahme(Kategorie kategorie, Bankkonto bankkonto, double wert, String waehrung, String bezeichnung, String beschreibung, String datum
			, int intervall, String zeitspanne, String absender) {
		super(bankkonto, kategorie, wert, waehrung, bezeichnung, beschreibung, datum, intervall, zeitspanne);
		this.absender = absender;
	}

	public String getAbsender() {
		return absender;
	}

	public void setAbsender(String absender) {
		this.absender = absender;
	}
	
	

}
