package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import db.KategorieDaoImpl;

/**
 * Fachklasse Kategorie
 */
@DatabaseTable(daoClass = KategorieDaoImpl.class, tableName = "kategorie")
public class Kategorie {

	@DatabaseField(generatedId = true)
	private long id;
	@DatabaseField(canBeNull = false, unique = true)
	private String bezeichnung;
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
	private Benutzer benutzer;

	/**
	 * Nicht benutzen.<br>
	 * Leerer Konstruktor f�r ORM Lite.
	 */
	Kategorie() {
		/* Leerer Konstruktor f�r ORM Lite. */}

	/**
	 * Konstruktor der Klasse Kategorie.
	 * @param benutzer    Ersteller der Kategorie Kategorie (Pflicht).
	 * @param bezeichnung Bezeichnung (Pflicht).
	 */
	public Kategorie(Benutzer benutzer, String bezeichnung) {
		this.bezeichnung = bezeichnung;
		this.benutzer = benutzer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public Benutzer getBenutzer() {
		return benutzer;
	}

	public void setBenutzer(Benutzer benutzer) {
		this.benutzer = benutzer;
	}

	public long getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		Kategorie other = (Kategorie) obj;
		return this.getBezeichnung().matches(other.getBezeichnung());
	}

	@Override
	public String toString() {
		return this.bezeichnung;
	}
}
