package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Fachklasse Ausgabe, Unterklasse von Transaktion.
 */
@DatabaseTable(tableName = "ausgabe")
public class Ausgabe extends Transaktion {
	
	@DatabaseField
	private String empfaenger;
	
	/**
	 * Nicht benutzen.<br>
	 * Leerer Konstruktor f�r ORM Lite.
	 */
	Ausgabe() {/*Leerer Konstruktor f�r ORM Lite.*/}
	
	/**
	 * Konstroktor der Klasse Ausgabe.
	 * @param bankkonto Bankkonto dem die Transaktion(Ausgabe) geh�rt (Pflicht).
	 * @param kategorie Kategorie der Transaktion(Ausgabe) (Pflicht).
	 * @param wert Wert der Transaktion(Ausgabe) (Pflicht).
	 * @param waehrung Waehrung (Pflicht).
	 * @param bezeichnung Bezeichnung (optional).
	 * @param beschreibung 	Beschreibung (optional).
	 * @param datum Datum an den die Transaktion(Ausgabe) gelaufen ist (optional).
	 * @param intervall Intervall f�r Regelmaessigkeit (optional).
	 * @param zeitspanne Zeitspanne (optional).
	 * @param empfaenger Empfaenger (optional).
	 */
	public Ausgabe(Kategorie kategorie, Bankkonto bankkonto, double wert, String waehrung, String bezeichnung, String beschreibung, String datum,
			int intervall, String zeitspanne, String empfaenger) {
		super(bankkonto, kategorie, wert, waehrung, bezeichnung, beschreibung, datum, intervall, zeitspanne);
		this.empfaenger = empfaenger;
	}

	public String getEmpfaenger() {
		return empfaenger;
	}

	public void setEmpfaenger(String empfaenger) {
		this.empfaenger = empfaenger;
	}
	
	
}
