package model;

import java.text.SimpleDateFormat;
import java.util.Collection;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import db.BenutzerDaoImpl;
import model.builders.BenutzerBuilder;

/**
 * Fachklasse Benutzer.
 */
@DatabaseTable(daoClass = BenutzerDaoImpl.class, tableName = "benutzer")
public class Benutzer {
	
	@DatabaseField(generatedId = true)
	private long id;
	@DatabaseField(canBeNull = false, unique = true)
	private String email;
	@DatabaseField(canBeNull = false)
	private String password;
	@DatabaseField
	private String nachname;
	@DatabaseField
	private String vorname;
	@DatabaseField
	private String gebDatum;
	@DatabaseField
	private String strasse;
	@DatabaseField
	private String hausNr;
	@DatabaseField
	private int plz;
	@DatabaseField
	private String ort;
	@DatabaseField
	private String land;
	@ForeignCollectionField
	private Collection<Bankkonto> bankkonten;
	@ForeignCollectionField
	private Collection<Kategorie> kategorien;

	Benutzer() { /*Leerer Konstruktor f�r ORM Lite*/}
	public Benutzer(BenutzerBuilder benutzerBuilder) {
		this.email = benutzerBuilder.getEmail();
		this.password = benutzerBuilder.getPasswort();
		this.nachname = benutzerBuilder.getNachname();
		this.vorname = benutzerBuilder.getVorname();
		this.gebDatum = benutzerBuilder.getGebDatum();
		this.strasse = benutzerBuilder.getStrasse();
		this.hausNr = benutzerBuilder.getHausNr();
		this.plz = benutzerBuilder.getPlz();
		this.ort = benutzerBuilder.getOrt();
		this.land = benutzerBuilder.getLand();
	}
	/**
	 * Konstruktor der Fachklasse Benutzer.
	 * @param email Email (Pflicht).
	 * @param password	Passwort (Pflicht).
	 * @param nachname Nachname (optional).
	 * @param vorname Vorname (optional).
	 * @param gebDatum Geburtsdatum (optional).
	 * @param strasse Strasse (optional).
	 * @param hausNr Hausnummer (optional).
	 * @param plz Postleitzahl (optional).
	 * @param ort Wohnort (optional).
	 * @param land Land (optional).
	 */
	public Benutzer(String email, String password, String nachname, String vorname, String gebDatum, String strasse,
			String hausNr, int plz, String ort, String land) {
		this.email = email;
		this.password = password;
		this.nachname = nachname;
		this.vorname = vorname;
		this.gebDatum = gebDatum;
		this.strasse = strasse;
		this.hausNr = hausNr;
		this.plz = plz;
		this.ort = ort;
		this.land = land;
	}
	
	public void setBankkonten(Collection<Bankkonto> bankkonten) {
		this.bankkonten = bankkonten;
	}
	
	public void setKategorien(Collection<Kategorie> kategorien) {
		this.kategorien = kategorien;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getGebDatum() {
		return gebDatum;
	}

	public void setGebDatum(String gebDatum) {
		this.gebDatum = gebDatum;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getHausNr() {
		return hausNr;
	}

	public void setHausNr(String hausNr) {
		this.hausNr = hausNr;
	}

	public int getPlz() {
		return plz;
	}

	public void setPlz(int plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}
	
	public void setOrt(String ort) {
		this.ort = ort;
	}
	
	public String getLand() {
		return land;
	}
	
	public void setLand(String land) {
		this.land = land;
	}
	
	public long getId() {
		return id;
	}
	
	public Collection<Bankkonto> getBankkonten() {
		return bankkonten;
	}
	
	public Collection<Kategorie> getKategorien() {
		return kategorien;
	}
	
	
}
