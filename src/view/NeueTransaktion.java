package view;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import controller.Plattform;
import model.Kategorie;

/**
 * Klasse fuer das Fenster zum hinzufuegen neuer Transaktionen.
 */
public class NeueTransaktion extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane; //Hauptlayout
	private Border border = BorderFactory.createLineBorder(Color.black); //Rahmen fuer die Textfelder
	
	private JComboBox<String> kategorie; 
	private JFormattedTextField betrag;
	private JComboBox<String> waehrung;
	private JTextField bezeichnung;
	private JTextField beschreibung;
	private JLabel label;
	private JLabel label2;
	private JFormattedTextField datum;
	private JComboBox<String> intervall;
	private JFormattedTextField datum2;
	
	private Plattform plattform; 
	private boolean einnahme; //true: Einnahme, false: Ausgabe
	private FinanzSoftware finanz;
	
	private List<Kategorie> kategorien = new LinkedList<Kategorie>(); //Liste aller Kategorien
	
	/**
	 * Konstruktor der Klasse NeueTransaktion.
	 * @param p Plattform, welche die Transaktionen abhandelt und Daten liefert.
	 * @param ein Boolean, der angibt ob es sich um eine Einnahme oder Ausgabe handelt.
	 * @param fs FinanzSoftware, die die Transaktion aufgerufen hat.
	 */
	public NeueTransaktion(Plattform p, boolean ein, FinanzSoftware fs) {
		einnahme = ein;
		plattform = p;
		finanz = fs;
		
		//Erstellen des Fensters und festlegung des Layouts.
		if(einnahme) setTitle("Neue Einnahme");
		else setTitle("Neue Ausgabe");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(450, 250);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GroupLayout layout = new GroupLayout(contentPane);
		contentPane.setLayout(layout);
		setVisible(true);
		
		//Erstellen der Kategorie ComboBox.
		kategorie = new JComboBox<String>();
		try {
			kategorien = plattform.getKategorien();
			for(Kategorie k : kategorien) {
				kategorie.addItem(k.getBezeichnung());
			}
		} catch (SQLException e) {
			System.out.println(e);
		}

		//Erstellen des Betrag Textfeldes.
		DecimalFormat format = new DecimalFormat("###,##0.00");
		betrag = new JFormattedTextField(format);
		betrag.setValue(0.00);
		betrag.setBorder(border);
		betrag.addMouseListener(new MouseAdapter() {
			  @Override
			  public void mouseClicked(MouseEvent e) {
				  betrag.selectAll();
			  }
		});

		//Erstellen der Waehrungs ComboBox.
		waehrung = new JComboBox<String>();
		waehrung.addItem("Euro");
		waehrung.addItem("Dollar");

		//Erstellen der Textfelder bezeichnung und Beschreibung.
		bezeichnung = new JTextField("Bezeichnung");
		bezeichnung.setBorder(border);
		bezeichnung.addMouseListener(new MouseAdapter() {
			  @Override
			  public void mouseClicked(MouseEvent e) {
				  bezeichnung.setText("");
				  bezeichnung.removeMouseListener(this);
			  }
		});
		beschreibung = new JTextField("Beschreibung");
		beschreibung.setBorder(border);
		beschreibung.addActionListener(click -> beschreibung.setText(""));
		beschreibung.addMouseListener(new MouseAdapter() {
			  @Override
			  public void mouseClicked(MouseEvent e) {
				  beschreibung.setText("");
				  beschreibung.removeMouseListener(this);
			  }
		});

		//Erstellen der Label.
		if(einnahme) label = new JLabel("Datum der Einnahme:");
		else label = new JLabel("Datum der Ausgabe:");
		label2 = new JLabel("bis:");
		
		//Erstellen der Datum Textfelder.
		datum = new JFormattedTextField(new Date());
		datum.setBorder(border);
		datum2 = new JFormattedTextField(new Date());
		datum2.setBorder(border);
		
		//Erstellen der Intervall ComboBox.
		intervall = new JComboBox<String>();
		intervall.addItem("Intervall");
		intervall.addItem("Einen Monat");
		intervall.addItem("Zwei Monate");
		intervall.addItem("Viertel Jahr");
		intervall.addItem("Halbes Jahr");
		intervall.addItem("Ein Jahr");
		
		//Erstellen der Buttons hinzufuegen und abbrechen.
		JButton hinzufuegen = new JButton("Hinzuf\u00FCgen");
		hinzufuegen.addActionListener(click -> this.erzeugTransaktion());
		JButton abbrechen = new JButton("Abbrechen");
		abbrechen.addActionListener(click -> this.dispose());

		//Aufbau des Layouts.
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		layout.setHorizontalGroup(
				layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
									.addComponent(kategorie)
									.addComponent(beschreibung)
									.addComponent(intervall)
								)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)		
									.addComponent(betrag)
									.addComponent(bezeichnung)
									.addComponent(datum2)
									.addComponent(label2)
								)
								.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
										.addComponent(waehrung)
										.addComponent(label)
										.addComponent(datum)
										
								)
						)
						.addGroup(layout.createSequentialGroup()
								.addGap(75)
								.addComponent(hinzufuegen)
								.addGap(25)
								.addComponent(abbrechen)
						)
				)
		);
		layout.setVerticalGroup(
				layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(kategorie)
						.addComponent(betrag)
						.addComponent(waehrung)
				)
				.addComponent(label)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(bezeichnung)
						.addComponent(beschreibung)
						.addComponent(datum)
				)
				.addComponent(label2)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(intervall)
						.addComponent(datum2)
				)
				.addGap(25)
				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(hinzufuegen)
						.addComponent(abbrechen)
				)
		);
	}

	/**
	 *  Liest die eingegebenen Daten ein und erstellt daraus eine neue Transaktion.
	 *  Erzeugt bei fehlerhafter Eingabe eine Meldung fuer einen Fehler und 
	 *  bei korekter Eingabe eine Meldung als Bestaetigung.
	 */
	private void erzeugTransaktion() {
		
		String wrng = "" + getSelectedWaehrung();
		
		String dt = datum.getText();
		String zeits = getEndDatum();
		String bes = beschreibung.getText().equals("Beschreibung") ? "" : beschreibung.getText();
		String bez = bezeichnung.getText().equals("Bezeichnung") ? "" : bezeichnung.getText();
		
		String empf = "";
		String abs = "";
		
		double wrt = Double.parseDouble(betrag.getValue().toString());
		
		int intv = getSelectedIntervall();
				
		if(wrt != 0.0) {
			try {
				Kategorie ktg = getSelectedKategorie();
				if(einnahme) {
					plattform.transaktionManager
					.addTransaktion("Einnahme", wrt, wrng, bez, bes, dt, ktg, intv, zeits, abs, empf);
					new Meldung("Bestätigung", "Die Einnahme wurde erfolgreich\nhinzugefügt.");	
				}else {
					plattform.transaktionManager
						.addTransaktion("Ausgabe", wrt, wrng, bez, bes, dt, ktg, intv, zeits, abs, empf);
					new Meldung("Bestätigung", "Die Ausgabe wurde erfolgreich\nhinzugefügt.");
				}
				finanz.update();
				this.dispose();
			} catch (SQLException e) {
				new Meldung("Fehler", "Transaktion konnte nicht\nabgeschlossen werde.\nDatenbankfehler");
				System.out.println(e);
			}
		} else {
			betrag.setBorder(BorderFactory.createLineBorder(Color.red));
			new Meldung("Fehler", "Es wurde ein Pflichtfeld\nausgelassen oder falsch\nbefüllt.");
		}
	}
	
	/**
	 * Ermittelt die gewaehlte Kategorie.
	 * @return gewaehlte Kategorie.
	 */
	private Kategorie getSelectedKategorie() {
		for(Kategorie k : kategorien) {
			if(k.getBezeichnung().equals(kategorie.getSelectedItem().toString())) {
				return k;
			}
		}
		return null;
	}
	
	/**
	 * Ermittelt das Waehrungszeichen fuer die gewählte Waehrung.
	 * @return Zeichen der Waehrung.
	 */
	private char getSelectedWaehrung() {
		String item = waehrung.getSelectedItem().toString();
		switch(item){
			case "Euro":
				return '€';
			case "Dollar":
				return '$';
			default:
				return '€';
		}
	}
	
	/**
	 * Erimittelt ob das zweite Datum nach dem Startdatum liegt.
	 * @return leerer String, wenn das Datum gleich oder vor dem Startdatum liegt, 
	 * 		sonst das Datum als String.
	 */
	private String getEndDatum() {
		Date d1 = (Date)datum.getValue();
		Date d2 = (Date)datum2.getValue();
		
		if(d1.toString().equals(d2.toString()) || d1.after(d2)) {
			return "";
		}

		return d2.toString();
	}
	
	/**
	 * Ermittelt welcher int Wert durch den Intervall gesetzt wird. 
	 * @return int fuer die Menge an Monaten.
	 */
	private int getSelectedIntervall() {
		String item = intervall.getSelectedItem().toString();
		switch(item){
			case "Einen Monat":
				return 1;
			case "Zwei Monate":
				return 2;
			case "Viertel Jahr":
				return 3;
			case "Halbes Jahr":
				return 6;
			case "Ein Jahr":
				return 12;
			default:
				return 0;
		}
	}
}