package view;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.JButton;

/**
 * Klasse fuer das Meldungsfenster.
 */
public class Meldung extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane; //Hauptlayout

	/**
	 * Konstruktor der Klasse Meldung.
	 * @param titel String, der den Titel des Fensters bestimmt.
	 * @param meldung String, der die Meldung des Fensters bestimmt.
	 */
	public Meldung(String titel, String meldung) {
		//Erstellen des Fensters und festlegung des Layouts.
		setType(Type.UTILITY);
		setTitle(titel);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(350, 200);
		setLocationRelativeTo(null);
		setVisible(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);

		//Erstellen des Textfeldes fuer die Meldung.
		JTextPane textfeld = new JTextPane();
		StyleContext.NamedStyle centerStyle = StyleContext.getDefaultStyleContext().new NamedStyle();
		StyleConstants.setAlignment(centerStyle, StyleConstants.ALIGN_CENTER);
		StyleConstants.setFontFamily(centerStyle, "Arial");
		textfeld.setLogicalStyle(centerStyle);
		textfeld.setText(meldung);
		textfeld.setEditable(false);
		contentPane.add(textfeld, BorderLayout.CENTER);
		
		//Erstellen des Buttens zum bestaetigen der Meldung.
		JButton button = new JButton("Ok");
		button.addActionListener(click -> this.dispose());
		contentPane.add(button, BorderLayout.SOUTH);
	}
} 