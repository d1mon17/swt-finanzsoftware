package view;

import java.awt.BorderLayout;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Group;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import controller.Plattform;
import model.Transaktion;

/**
 * Erzeugt die Hauptbenutzeroberflaeche der Software.
 */
public class FinanzSoftware extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane; // Hauptlayout
	private JTable table;
	private Plattform plattform;
	private JLabel kontostandReal;
	private JLabel kontostandBerechnet;
	JPanel unten;
	JScrollPane scrollPane;
	Panel kontostand;

	// Spaltennamen der Tabelle
	String[] colNames = new String[] { "Datum", "Kategorie", "Bezeichnung", "Wert", "Optionen" };
	private Object[][] data; // Daten fuer die Tabelle

	/**
	 * Konstruktor fuer die Klasse FinanzSoftware.
	 */
	public FinanzSoftware() {
		// Erstellen des Fensters und festlegung des Layouts.
		setTitle("FinanzSoftware");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(700, 750);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel rechts = new JPanel();
		rechts.setBackground(Color.WHITE);
		contentPane.add(rechts, BorderLayout.LINE_END);
		GroupLayout gL = new GroupLayout(rechts);
		rechts.setLayout(gL);
		gL.setAutoCreateGaps(true);
		gL.setAutoCreateContainerGaps(true);

		Group horizontal = gL.createSequentialGroup();
		Group hpara = gL.createParallelGroup(GroupLayout.Alignment.LEADING);
		Group vertical = gL.createSequentialGroup();

		gL.setHorizontalGroup(horizontal);
		gL.setVerticalGroup(vertical);
		horizontal.addGroup(hpara);

		kontostand = new Panel();
		kontostand.setLayout(new GridLayout(7, 0, 0, 0));
		hpara.addComponent(kontostand);
		vertical.addComponent(kontostand);

		kontostandReal = new JLabel("Kontostand Real:");
		kontostand.add(kontostandReal);

		JLabel platzhalter = new JLabel("");
		kontostand.add(platzhalter);

		kontostandBerechnet = new JLabel("Kontostand berechnet:");
		kontostand.add(kontostandBerechnet);

		Choice konto = new Choice();
		kontostand.add(konto);

		JButton kontostandEin = new JButton("Kontostand eingabe");
		kontostand.add(kontostandEin);

		JButton differenz = new JButton("Differenz ausgleichen");
		kontostand.add(differenz);

		Panel transaktion = new Panel();
		hpara.addComponent(transaktion);
		vertical.addComponent(transaktion);
		transaktion.setLayout(new GridLayout(5, 0, 0, 0));

		JButton neuAusgabe = new JButton("Neue Ausgabe");
		neuAusgabe.addActionListener(click -> new NeueTransaktion(plattform, false, this));
		transaktion.add(neuAusgabe);

		JButton neuEinnahme = new JButton("Neue Einnahme");
		neuEinnahme.addActionListener(click -> new NeueTransaktion(plattform, true, this));
		transaktion.add(neuEinnahme);

		Panel filterRechts = new Panel();
		hpara.addComponent(filterRechts);
		vertical.addComponent(filterRechts);
		filterRechts.setLayout(new GridLayout(0, 2, 0, 0));

		JButton filter = new JButton("Filter");
		filterRechts.add(filter);

		JButton filterzuruecksetzen = new JButton("Filter Zur\u00FCcksetzen");
		filterRechts.add(filterzuruecksetzen);

		JPanel links = new JPanel();
		links.setBackground(Color.WHITE);
		contentPane.add(links, BorderLayout.LINE_START);
		links.setLayout(new BorderLayout(0, 0));

		JMenuBar menuBar = new JMenuBar();
		links.add(menuBar, BorderLayout.PAGE_START);

		JMenu extras = new JMenu("Extras");
		menuBar.add(extras);

		JLabel Platzhalter1 = new JLabel("Platzhalter");
		extras.add(Platzhalter1);

		JMenu bankkonto = new JMenu("Bankkonto");
		menuBar.add(bankkonto);

		JLabel Platzhalter2 = new JLabel("Platzhalter");
		bankkonto.add(Platzhalter2);

		JMenu user = new JMenu("User");
		menuBar.add(user);

		JLabel Platzhalter3 = new JLabel("Platzhalter");
		user.add(Platzhalter3);

		BufferedImage diagr;
		try {
			diagr = ImageIO.read(new File(new File("").getAbsolutePath() + "\\src\\resources\\bsp.jpeg"));
			JLabel diagramm = new JLabel(new ImageIcon(diagr));
			links.add(diagramm, BorderLayout.CENTER);
		} catch (IOException e) {
			System.err.println(e);
		}

		JPanel filterLinks = new JPanel();
		filterLinks.setBackground(Color.WHITE);
		links.add(filterLinks, BorderLayout.PAGE_END);
		filterLinks.setLayout(new FlowLayout());

		JButton einaus = new JButton("Einnahme/Ausgabe");
		einaus.addActionListener(click -> filterEinAusgabe(true, true));
		filterLinks.add(einaus);

		JButton ein = new JButton("Einnahme");
		ein.addActionListener(click -> filterEinAusgabe(true, false));
		filterLinks.add(ein);

		JButton aus = new JButton("Ausgabe");
		aus.addActionListener(click -> filterEinAusgabe(false, true));
		filterLinks.add(aus);

		unten = new JPanel();
		contentPane.add(unten, BorderLayout.PAGE_END);
		unten.setLayout(new BorderLayout(0, 0));
		unten.setPreferredSize(new Dimension(500, 250));

		table = new JTable(data, colNames);
		unten.add(table, BorderLayout.CENTER);

		scrollPane = new JScrollPane(table);
		unten.add(scrollPane);
	}

	/**
	 * Setzt die zu benutzende Plattform und aktualisiert das Fenster.
	 * 
	 * @param plattform Die zu benutzende Plattform.
	 */
	public void start(Plattform plattform) {
		this.plattform = plattform;
		this.update();
		this.setVisible(true);
	}

	/**
	 * Aktualisiert die alle Daten und das Fenster.
	 */
	public void update() {
		int i = 0;
		List<Transaktion> list = plattform.transaktionManager.getTransaktionen();
		unten.remove(scrollPane);
		unten.remove(table);
		data = new Object[list.size()][5];

		for (Transaktion t : list) {
			data[i][0] = t.getDatum();
			data[i][1] = t.getKategorie();
			data[i][2] = t.getBezeichnung();
			data[i][3] = t.getWert() + t.getWaehrung();
			data[i][4] = "Buttons";
			i++;
		}

		table = new JTable(data, colNames);
		unten.add(table);
		table.getTableHeader().setReorderingAllowed(false);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);

		scrollPane = new JScrollPane(table);
		unten.add(scrollPane);

		kontostandReal.setText("Kontostand Real: " + plattform.kontoManager.getRealKontostand());
		kontostandBerechnet.setText("Kontostand berechnet: " + plattform.kontoManager.getBerechnetKontostand());
	}

	/**
	 * Filter die Tabelle nach Einnahme und/oder Ausgabe. Wenn beide Eingaben false
	 * werden Einnahmen und Ausgaben gelistet.
	 * 
	 * @param ein Boolean, der angibt ob die Einnahmen mit gelistet werden sollen.
	 * @param aus Boolean, der angibt ob die Ausgaben mit gelistet weden sollen.
	 */
	private void filterEinAusgabe(boolean ein, boolean aus) {
		int i = 0;
		List<Transaktion> list = new LinkedList<Transaktion>();

		if (!ein && aus) {
			list = plattform.transaktionManager.getFilterTransaktion("Ausgabe", "", null, "", "");
		} else if (ein && !aus) {
			list = plattform.transaktionManager.getFilterTransaktion("Einnahme", "", null, "", "");
		} else {
			list = plattform.transaktionManager.getTransaktionen();
		}

		unten.remove(table);
		unten.remove(scrollPane);
		data = new Object[list.size()][5];
		for (Transaktion t : list) {
			data[i][0] = t.getDatum();
			data[i][1] = t.getKategorie();
			data[i][2] = t.getBezeichnung();
			data[i][3] = t.getWert() + t.getWaehrung();
			data[i][4] = "Buttons";
			i++;
		}
		table = new JTable(data, colNames);
		unten.add(table);
		table.getTableHeader().setReorderingAllowed(false);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);
		scrollPane = new JScrollPane(table);
		unten.add(scrollPane);

		return;
	}
}
